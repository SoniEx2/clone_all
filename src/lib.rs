/// Clones all the given variables into new variables. Intended for use with `move` closures.
///
/// # Examples
///
/// Compare this:
///
/// ```rust
/// # use std::sync::Arc;
/// # use std::sync::Mutex;
/// # use std::thread;
/// # use std::collections::HashSet;
/// 
/// # fn main() {
/// let i = Arc::new(Mutex::new(String::from("hello world")));
/// let o = Arc::new(Mutex::new(String::new()));
/// for _ in "hello world".chars() {
///     thread::spawn({let i = i.clone(); let o = o.clone(); move || {
///         let c = i.lock().unwrap().pop();
///         let c = c.unwrap();
///         o.lock().unwrap().push(c);
///     }}).join();
/// }
/// # assert_eq!("dlrow olleh", &o.lock().unwrap().clone());
/// # }
/// ```
///
/// To this:
///
/// ```rust
/// #[macro_use]
/// extern crate clone_all;
/// # use std::sync::Arc;
/// # use std::sync::Mutex;
/// # use std::thread;
/// # use std::collections::HashSet;
/// 
/// # fn main() {
/// let i = Arc::new(Mutex::new(String::from("hello world")));
/// let o = Arc::new(Mutex::new(String::new()));
/// for _ in "hello world".chars() {
///     thread::spawn({clone_all!(i, o); move || {
///         let c = i.lock().unwrap().pop();
///         let c = c.unwrap();
///         o.lock().unwrap().push(c);
///     }}).join();
/// }
/// # assert_eq!("dlrow olleh", &o.lock().unwrap().clone());
/// # }
/// ```
#[macro_export]
macro_rules! clone_all {
    ($($i:ident),+) => {
        $(let $i = $i.clone();)+
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_clone_string() {
        let s: String = "hello world".into();
        {
            clone_all!(s);
            ::std::mem::drop(s);
        }
        ::std::mem::drop(s);
    }

    #[test]
    fn test_clone_multiple_strings() {
        let s1: String = "hello world".into();
        let s2: String = "goodbye".into();
        {
            clone_all!(s1, s2);
            ::std::mem::drop(s1);
            ::std::mem::drop(s2);
        }
        ::std::mem::drop(s1);
        ::std::mem::drop(s2);
    }
}
